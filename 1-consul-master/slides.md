%title: MESH
%author: xavki

# MESH

<br>
introduction : cf vidéo notion

<br>
utilisation de Vagrant : test, recette...

<br>
MESH = système registry + reverse-proxy sidecar + service

```
                            +-------------------------+
          +-----------------+  Registry Services      +--------------+
          |                 +-------------------------+              |
    +---------------------------------+        +--------------------------------+
    |     +                           |        |                     +          |
    |   agent            service      |        |    service      ++ agent       |
    |   registry           +          |        |    +            |  registry    |
    |      +               |          |        |    |            |              |
    |      |               +          |        |    +            |              |
    |      |                          |        |                 |              |
    |      +--+ Reverse-Proxy         |        |  Reverse-Proxy  +              |
    |                 ++              |        |        ++                      |
    +---------------------------------+        +--------------------------------+
                      |                                 |
                      +---------------------------------+
                                       ^
                                       |
                                       |
                                    +-----+
                                    |     |
                                    +-----+

```

---------------------------------------------------------------------------------------

# Création du master


```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # cmaster server
  config.vm.define "cmaster" do |cmaster|
    cmaster.vm.box = "ubuntu/bionic64"
    cmaster.vm.hostname = "cmaster"
    cmaster.vm.box_url = "ubuntu/bionic64"
    cmaster.vm.network :private_network, ip: "192.168.58.10"
    cmaster.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--memory", 1024]
      v.customize ["modifyvm", :id, "--name", "cmaster"]
      v.customize ["modifyvm", :id, "--cpus", "1"]
    end
    config.vm.provision "shell", inline: <<-SHELL
      sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config    
      service ssh restart
    SHELL
    cmaster.vm.provision "shell", path: "install_cmaster.sh"
  end
end
```

