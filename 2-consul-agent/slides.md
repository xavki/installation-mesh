%title: MESH
%author: xavki

# MESH

<br>
```
                            +-------------------------+
          +-----------------+  Registry Services      +--------------+
          |                 +-------------------------+              |
    +---------------------------------+        +--------------------------------+
    |     +                           |        |                     +          |
    |   agent            service      |        |    service      ++ agent       |
    |   registry           +          |        |    +            |  registry    |
    |      +               |          |        |    |            |              |
    |      |               +          |        |    +            |              |
    |      |                          |        |                 |              |
    |      +--+ Reverse-Proxy         |        |  Reverse-Proxy  +              |
    |                 ++              |        |        ++                      |
    +---------------------------------+        +--------------------------------+
                      |                                 |
                      +---------------------------------+
                                       ^
                                       |
                                       |
                                    +-----+
                                    |     |
                                    +-----+

```

---------------------------------------------------------------------------------------

# Création des consul agent


```
numberSrv=3
  # myapp server
  (1..numberSrv).each do |i|
    config.vm.define "myapp#{i}" do |app|
      myapp.vm.box = "ubuntu/bionic64"
      myapp.vm.hostname = "myapp#{i}"
      myapp.vm.network "private_network", ip: "192.168.57.1#{i}"
      myapp.vm.provider "virtualbox" do |v|
        v.name = "myapp#{i}"
        v.memory = 1024
        v.cpus = 1
      end
      config.vm.provision "shell", inline: <<-SHELL
        sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
        service ssh restart
      SHELL
      app.vm.provision "shell", path: "install_myapp.sh"
    end
  end
```

